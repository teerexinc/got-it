package mcca.config;

/**
 * Created by Dell on 02/11/2015.
 */

public class Route {

    // API Mappings
    public static final String BASE_API_URL = "/api";
    public static final String API_SINGLE_ENTITY_URL = "/{id}"; // GET, PUT, DELETE

    public static final String TEEN_API_BASE_URL = "/teen"; // GET
    public static final String FOLLOWER_API_BASE_URL = "/follower"; // GET
    public static final String USER_API_BASE_URL = "/user"; // GET

    public static final String USER_API_URL = BASE_API_URL + USER_API_BASE_URL;
    public static final String TEEN_API_URL = BASE_API_URL + TEEN_API_BASE_URL;
    public static final String FOLLOWER_API_URL = BASE_API_URL + FOLLOWER_API_BASE_URL;
}
