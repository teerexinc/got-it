package mcca.repository;

/**
 * Created by Dell on 06/11/2015.
 */

import mcca.model.Follower;
import mcca.model.Teen;
import mcca.model.User;
import org.springframework.data.repository.CrudRepository;

public interface TeenRepository extends CrudRepository<Teen, Long> {
    Teen findByUser(User user);
}