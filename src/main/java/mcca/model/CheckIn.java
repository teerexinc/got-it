package mcca.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dell on 10/11/2015.
 */

@Entity
@Table(name = "check_ins")
public class CheckIn extends BaseModel {

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "teen_id", nullable = true)
    private Teen teen;

    @Column
    private Date mealTime;

    @Column
    private Date bedTime;
}
