package mcca.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Created by Dell on 06/11/2015.
 */

@MappedSuperclass
public abstract class AppUser extends BaseModel {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    private boolean isTeen;

    @OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "user_id", nullable = true)
    private User user;

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isTeen() {
        return isTeen;
    }

    public void setIsTeen(boolean isTeen) {
        this.isTeen = isTeen;
    }
}
