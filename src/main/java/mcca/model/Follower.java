package mcca.model;

/**
 * Created by Dell on 01/11/2015.
 */

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "followers")
public class Follower extends AppUser {

    @ManyToMany
    @JoinTable(name = "teen_follower",
            joinColumns = @JoinColumn(name = "teen_id"),
            inverseJoinColumns = @JoinColumn(name = "follower_id"))
    private Collection<Teen> teens;

    public Collection<Teen> getTeens() {
        return teens;
    }

    public void setTeens(Collection<Teen> teens) {
        this.teens = teens;
    }
}
