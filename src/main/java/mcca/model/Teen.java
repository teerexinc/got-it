package mcca.model;

/**
 * Created by Dell on 01/11/2015.
 */

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "teens")
public class Teen extends AppUser {

    @Column
    private Date dob;

    @Column
    private long mrn;// medical record number

    @ManyToMany(mappedBy = "teens")
    private Collection<Follower> followers;

    public Date getDob() { return dob; }

    public void setDob(Date dob) { this.dob = dob; }

    public long getMrn() { return mrn; }

    public void setMrn(long mrn) { this.mrn = mrn; }

    public Collection<Follower> getFollowers() { return followers; }

    public void setFollowers(Collection<Follower> followers) {
        this.followers = followers;
    }

    public void addFollower(Follower follower){ followers.add(follower); }

    public boolean removeFollower(Follower follower){ return followers.remove(follower); }

}
