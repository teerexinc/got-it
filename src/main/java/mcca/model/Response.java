package mcca.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by taiwo.a.otubamowo on 17/11/2015.
 */

@Entity
@Table(name = "responses")
public class Response extends BaseModel {

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="question_id", nullable = true)
    private Question question;

    private String literal;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="check_in_id")
    private CheckIn checkIn;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public CheckIn getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(CheckIn checkIn) {
        this.checkIn = checkIn;
    }
}
