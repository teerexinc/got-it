package mcca.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by taiwo.a.otubamowo on 17/11/2015.
 */

@Entity
@Table(name = "questions")
public class Question extends BaseModel {

    @NotEmpty
    private String type;

    @NotEmpty
    private String literal;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }
}
