/*
 * Copyright 2014-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mcca.controller;

import mcca.client.UserService;
import mcca.config.Route;
import mcca.model.Teen;
import mcca.model.User;
import mcca.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = Route.USER_API_URL)
public class UserController {

	private final UserRepository userRepository;

	@Autowired
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@ResponseBody
	@RequestMapping(value = "all", method = RequestMethod.GET,
			produces = "application/json")
	public Iterable<User> getUsers() {
		return userRepository.findAll();
	}

	@ResponseBody
	@RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.GET,
			produces = "application/json")
	public User getUser(@PathVariable Long id) {
		return userRepository.findOne(id);
	}


	@ResponseBody
	@RequestMapping(method = RequestMethod.POST,
			produces = "application/json")
	public User saveTeen(@RequestBody User user) throws Exception {
		return userRepository.save(user);
	}
}
