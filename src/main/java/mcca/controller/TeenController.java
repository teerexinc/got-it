package mcca.controller;

/**
 * Created by Dell on 03/11/2015.
 */

import mcca.config.Route;
import mcca.model.Follower;
import mcca.model.Teen;
import mcca.model.User;
import mcca.repository.FollowerRepository;
import mcca.repository.TeenRepository;
import mcca.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = Route.TEEN_API_URL)
public class TeenController {

    private final TeenRepository teenRepository;
    private final UserRepository userRepository;
    private final FollowerRepository followerRepository;

    @Autowired
    public TeenController(TeenRepository teenRepository, FollowerRepository followerRepository, UserRepository userRepository) {
        this.teenRepository = teenRepository;
        this.followerRepository = followerRepository;
        this.userRepository = userRepository;
    }

    @ResponseBody
    @RequestMapping(value = "all", method = RequestMethod.GET,
            produces = "application/json")
    public Iterable<Teen> getTeens() throws Exception {
        return teenRepository.findAll();
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL + "/followers", method = RequestMethod.GET,
            produces = "application/json")
    public Collection<Follower> getTeenFollowers(@PathVariable Long id) throws Exception {
        return teenRepository.findOne(id).getFollowers();
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.DELETE,
            produces = "application/json")
    public Teen deleteTeen(@PathVariable Long id) throws Exception {
        teenRepository.delete(id);
        return new Teen();
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.GET,
            produces = "application/json")
    public Teen getTeen(@PathVariable Long id) throws Exception {
        return teenRepository.findOne(id);
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.PUT,
            produces = "application/json")
    public Teen updateTeen(@RequestBody Teen teenWithNewValues) throws Exception {
        teenRepository.save(teenWithNewValues);
        return teenWithNewValues;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    public Teen saveTeen(@AuthenticationPrincipal User user, @RequestBody Teen teen) throws Exception {
        user = userRepository.findOne(user.getId());
        teen.setUser(user);
        teenRepository.save(teen);
        return teen;
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL + "/follow", method = RequestMethod.PUT,
            produces = "application/json")
    public Teen followTeen(@AuthenticationPrincipal User user, @PathVariable Long id) throws Exception {
        user = userRepository.findOne(user.getId());
        Follower follower = followerRepository.findByUser(user);
        Teen teen = teenRepository.findOne(id);
        teen.addFollower(follower);
        return teen;
    }

}
