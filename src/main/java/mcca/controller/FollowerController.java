package mcca.controller;

/**
 * Created by Dell on 03/11/2015.
 */

import mcca.config.Route;
import mcca.model.Follower;
import mcca.model.User;
import mcca.repository.FollowerRepository;
import mcca.repository.TeenRepository;
import mcca.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = Route.FOLLOWER_API_URL)
public class FollowerController {

    private final TeenRepository teenRepository;
    private final UserRepository userRepository;
    private final FollowerRepository followerRepository;

    @Autowired
    public FollowerController(TeenRepository teenRepository, FollowerRepository followerRepository, UserRepository userRepository) {
        this.teenRepository = teenRepository;
        this.followerRepository = followerRepository;
        this.userRepository = userRepository;
    }

    @ResponseBody
    @RequestMapping(value = "all", method = RequestMethod.GET,
            produces = "application/json")
    public Iterable<Follower> getFollowers() throws Exception {
        return followerRepository.findAll();
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.DELETE,
            produces = "application/json")
    public Follower deleteFollower(@PathVariable Long id) throws Exception {
        followerRepository.delete(id);
        return new Follower();
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.GET,
            produces = "application/json")
    public Follower getFollower(@PathVariable Long id) throws Exception {
        return followerRepository.findOne(id);
    }

    @ResponseBody
    @RequestMapping(value = Route.API_SINGLE_ENTITY_URL, method = RequestMethod.PUT,
            produces = "application/json")
    public Follower updateFollower(@RequestBody Follower followerWithNewValues) throws Exception {
        followerRepository.save(followerWithNewValues);
        return followerWithNewValues;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    public Follower saveFollower(@AuthenticationPrincipal User user, @RequestBody Follower follower) throws Exception {
        user = userRepository.findOne(user.getId());
        follower.setUser(user);
        followerRepository.save(follower);
        return follower;
    }
}
