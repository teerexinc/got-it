package mcca.client;

import mcca.config.Route;
import mcca.model.Teen;
import mcca.model.User;
import retrofit.Call;
import retrofit.http.*;

/**
 * Created by taiwo.a.otubamowo on 15/11/2015.
 */
public interface UserService {

    // user service
    @GET(Route.USER_API_URL + "/all")
    Iterable<User> getUsers();

    @GET(Route.USER_API_URL + Route.API_SINGLE_ENTITY_URL)
    User getUser(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @POST(Route.USER_API_URL)
    Call<User> saveUser(@Body User user);

}
