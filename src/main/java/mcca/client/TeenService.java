package mcca.client;

import mcca.config.Route;
import mcca.model.Follower;
import mcca.model.Teen;
import retrofit.Call;
import retrofit.http.*;

import java.util.List;

/**
 * Created by taiwo.a.otubamowo on 15/11/2015.
 */
public interface TeenService {

    //teen service
    @GET(Route.TEEN_API_URL + "/all")
    Call<Iterable<Teen>> getTeens();

    @GET(Route.TEEN_API_URL + Route.API_SINGLE_ENTITY_URL + "/followers")
    Call<Iterable<Follower>> getTeenFollowers(@Path("id") Long id);

    @DELETE(Route.TEEN_API_URL + Route.API_SINGLE_ENTITY_URL)
    Call<Teen> deleteTeen(@Header("Authorization") String auth_token, @Path("id") Long id);

    @GET(Route.TEEN_API_URL + Route.API_SINGLE_ENTITY_URL)
    Call<Teen> getTeen(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @PUT(Route.TEEN_API_URL + Route.API_SINGLE_ENTITY_URL)
    Call<Teen> updateTeen(@Header("Authorization") String auth_token, @Body Teen teen);

    @Headers("Content-Type: application/json")
    @POST(Route.TEEN_API_URL)
    Call<Teen> saveTeen(@Header("Authorization") String auth_token, @Body Teen teen);

    @PUT(Route.API_SINGLE_ENTITY_URL + "/follow")
    Call<Teen> followTeen(@Header("Authorization") String auth_token, @Path("id") Long id);

}
