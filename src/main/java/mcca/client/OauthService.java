package mcca.client;

import retrofit.Call;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by taiwo.a.otubamowo on 14/11/2015.
 */
public interface OauthService {

    // oauth service
    @POST("/oauth/token")
    Call<Object> getToken
    (
            @Header("Authorization") String auth_token,
            @Path("grant_type") String grant_type,
            @Path("scope") String scope,
            @Path("username") String username,
            @Path("password") String password
    );

}