package mcca.client;

import mcca.config.Route;
import mcca.model.Follower;
import retrofit.Call;
import retrofit.http.*;

import java.util.List;

/**
 * Created by taiwo.a.otubamowo on 15/11/2015.
 */
public interface FollowerService {

    //follower service
    @GET(Route.FOLLOWER_API_URL + "/all")
    Call<List<Follower>> getFollowers();

    @DELETE(Route.FOLLOWER_API_URL + Route.API_SINGLE_ENTITY_URL)
    Call<Follower> deleteFollower(@Header("Authorization") String auth_token, @Path("id") Long id);

    @GET(Route.FOLLOWER_API_URL + Route.API_SINGLE_ENTITY_URL)
    Call<Follower> getFollower(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @PUT(Route.FOLLOWER_API_URL + Route.API_SINGLE_ENTITY_URL)
    Call<Follower> updateFollower(@Header("Authorization") String auth_token, @Body Follower follower);

    @Headers("Content-Type: application/json")
    @POST(Route.FOLLOWER_API_URL)
    Call<Follower> saveFollower(@Header("Authorization") String auth_token, @Body Follower follower);

}
