insert into users(id, login, password, deleted) values (1,'roy','spring', FALSE);
insert into users(id, login, password, deleted) values (2,'craig','spring', FALSE);
insert into users(id, login, password, deleted) values (3,'greg','spring', FALSE);

insert into users(id, login, password, deleted) values (4,'tee','android', FALSE);

insert into roles(id, name, deleted) values (1,'ROLE_USER', FALSE);
insert into roles(id, name, deleted) values (2,'ROLE_ADMIN', FALSE);
insert into roles(id, name, deleted) values (3,'ROLE_GUEST', FALSE);

insert into user_role(user_id, role_id) values (1,1);
insert into user_role(user_id, role_id) values (1,2);
insert into user_role(user_id, role_id) values (2,1);
insert into user_role(user_id, role_id) values (3,3);
insert into user_role(user_id, role_id) values (4,1);